clear all;
clc;
close all;

%The major variables
vl = 2;
vs = 0.01;
vu = 3;
alpha = 1.4:0.01:1.45;
phi_0 = 0;
dt = 0.0001;

global tau c1 c2 c3 c4 c5 c6 f g h;

%Global variables
tau = 0.5;
c1 = 0.2;
c2 = -0.1;
c3 = 0.1;
c4 = -1;
c5 = -0.6;
c6 = -0.02;

intersections = [];
count = 0;
for a=alpha
    %compute some intermediate values to tidy things up
    f = c1*sin(a) + c2*cos(a);
    g = sin(a)*(...
        c3*(sin(a)^2) +...
        (c4+c2)*sin(a)*cos(a) -...
        0.5*c1*(cos(a)^2)...
        );
    h = c6*(sin(a)^2) - (c5/6)*(cos(a)^2);

    [omega_0, omega_1] = poonValues(vl, vs, vu, a, phi_0, dt);
    %plot(x, y, '.', [x(1), x(end)], [x(1), x(end)], '-');
    [index1, index2] = bisect(omega_0, omega_1);
    %Take the average value
    x = 0.5*(omega_0(index1)+omega_0(index2));
    intersections = [intersections x];
    disp(count/length(alpha));
    count=count+1;
end

plot(alpha, intersections, '.');
xlabel('\alpha');
ylabel('Stable limit cycle, \omega (rad/s)');
title('Plot of Stable limit cycle against \alpha');


function [x1, x2] = bisect(x, y)
    %
    %Calculates the range for which the graph crosses y=x
    %x1 and x2 are the indicies for the list x for this range
    %
    
    di = -0.5*length(x);    %first change will be backwards to halfway
    i = length(x);                    %start at the right of the graph
    if y(i)>x(i)
        state = true;          %true if y>x at right of graph
    else
        state = false;
    end
    
    while abs(di)>1
        i = round(i + di, 0);
        if y(i)>x(i) && state==true
            %Test value is same as initial value
            %i.e., need to move halfway in the same direction
            di = 0.5*di;
        elseif y(i)<x(i) && state==false
            %Test value is same as initial value
            %i.e., need to move halfway in the same direction
            di = 0.5*di;
        else
            %Test value is not the same as initial value
            %i.e., need to move halfway back in the other direction
            di = -0.5*di;
            state = ~state;
        end
    end
    
    if di>0
        %Was moving to the right
        x1 = i;
        x2 = i+1;
    else
        %Was moving to the left
        x1 = i-1;
        x2 = i;
    end
    
end