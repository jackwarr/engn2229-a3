function pooncarie(alpha,vl,vs,vu,dt,yl,yu,ax)

global tau c1 c2 c3 c4 c5 c6 f g h;

%Global variables
tau = 0.5;
c1 = 0.2;
c2 = -0.1;
c3 = 0.1;
c4 = -1;
c5 = -0.6;
c6 = -0.02;

%compute some intermediate values to tidy things up
f = c1*sin(alpha) + c2*cos(alpha);
g = sin(alpha)*(...
    c3*(sin(alpha)^2) +...
    (c4+c2)*sin(alpha)*cos(alpha) -...
    0.5*c1*(cos(alpha)^2)...
    );
h = c6*(sin(alpha)^2) - (c5/6)*(cos(alpha)^2);

[w_0, omega_final] = poonValues(vl, vs, vu, alpha, 0, dt);

%plot(ax, w_0, omega_final, '.', 'MarkerSize', 1);
plot(w_0, omega_final, '.', [w_0(1), w_0(end)], [w_0(1), w_0(end)], '-');
%hold on;
%plot(w_0, w_0, 'red');
%plot(w_0, zeros(1,length(w_0)), 'black');
%hold off;
%set(ax, 'XLim', [vl vu], 'YLim', [yl yu]);
%ylim([vl, vu]);
xlabel('Initial Value of w_0')
ylabel('Final Value of \omega')
%title(sprintf('Poincare Map - a=pi/%.0d', pi/alpha))
title('Poincare Map');

end













