%
%Jack Gooday (u6378778) and Jack Warr (u6077299)
%Australian National University
%27th October 2018
%ENGN2229 Assignment 3
%

function omega = trajectory_quit(a, phi_0, w_0, dt)
%
%Computes a trajectory. Requires initial conditions
%phi_0 and w_0. Runs until phi=phi_0 again
%

% B L O C K   1 :  S E T U P

global tau c5 f g h

phi = phi_0;
omega = w_0;

%Nothing happens if phi_0=omega_0=0, so just return 0
if phi_0==0 && w_0==0
    return
end

% B L O C K   2 :   I N T E G R A T I O N   S T E P   1 
%Euler method for numerical integration of ODEs
while phi>=0
    %phi must always start at 0
    phi_dot = omega;
    x = tau*(f + g*(phi^2))*omega;
    y = sin(a)*(c5 + h*(phi^2))*phi;
    omega_dot = x + y;
    
    phi = phi + dt*phi_dot;
    omega =  omega + dt*omega_dot;
    
    %Reset angle if larger than half a revolution
    if phi>pi
        phi = 0;
    elseif phi<-pi
        phi = 0;
    end
    
end

% B L O C K   3 :   I N T E G R A T I O N   S T E P   2
while phi<0
    %phi needs to go positive->negative->0 again
    phi_dot = omega;
    x = tau*(f + g*(phi^2))*omega;
    y = sin(a)*(c5 + h*(phi^2))*phi;
    omega_dot = x + y;
    
    phi = phi + dt*phi_dot;
    omega =  omega + dt*omega_dot;
    
    if phi>pi
        phi = 0;
    elseif phi<-pi
        phi = 0;
    end
end

end