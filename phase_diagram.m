%This code will determine the phase portraits

clear all;
close all;
clc;

global tau c1 c2 c3 c4 c5 c6 f g h;

%Global variables
tau = 0.5;
c1 = 0.2;
c2 = -0.1;
c3 = 0.1;
c4 = -1;
c5 = -0.6;
c6 = -0.02;

%We have a variable angle of attack that needs to be iterated over
%(eventually) --- for now pick a value
a = 1.45;
phi_0 = 0;
omega_0 = 0.1;
dt = 0.001;

%compute some intermediate values to tidy things up
f = c1*sin(a) + c2*cos(a);
g = sin(a)*(...
    c3*(sin(a)^2) +...
    (c4+c2)*sin(a)*cos(a) -...
    0.5*c1*(cos(a)^2)...
    );
h = c6*(sin(a)^2) - (c5/6)*(cos(a)^2);

t_max = 100;
[phi, omega] = trajectory(a, phi_0, omega_0, dt, t_max);
vectorfield1(a, 20);
plot_traj(phi, omega)
title('Roll dynamic 6: instability');

%For plotting phi against t
%t = 1:dt:t_max;
%plot(t, phi);
%xlabel('Time (s)');
%ylabel('\phi (rad)');
%title(sprintf('Angle of aircraft over time\nExtreme attack angle'));

%trajectory_quit(a, phi_0, omega_0, dt)
%sprintf('Vector Field of the Roll Dynamics, a = pi/%.0f', pi/a)


function vectorfield1(a, density)
%
%Plots the vector field
%

global tau c5 f g h;

%Make grid of variables
phi_val = linspace(-pi/2, pi/2, density);
omega_val = linspace(-1, 1, density);
[phi, omega] = meshgrid(phi_val, omega_val);
%Empty arrays for derivatives
phi_dot = zeros(size(phi));
omega_dot = zeros(size(omega));

%Calculate derivatives for grid
for i = 1:numel(phi)
    phi_dot(i) = omega(i);
    x = tau*(f + g*(phi(i)^2))*omega(i);
    y = sin(a)*(c5 + h*(phi(i)^2))*phi(i);
    omega_dot(i) = x + y;
end 

%Plot field
hold on;
quiver(phi, omega, phi_dot, omega_dot, 'red');
axis equal;
xlabel('Angle \phi');
ylabel('Angular Velocity \omega');
hold off;

end 


function vectorfield2(a, density)
%
%Plots the vector field
%

global tau c5 f g h;

%Make grid of variables
phi_val = linspace(-pi, pi, density);
omega_val = linspace(-1, 1, density);
[phi, omega] = meshgrid(phi_val, omega_val);
%Empty arrays for derivatives
phi_dot = zeros(size(phi));
omega_dot = zeros(size(omega));

%Calculate derivatives for grid
for i = 1:numel(phi)
    phi_dot(i) = omega(i);
    x = tau*(f + g*(phi(i)^2))*omega(i);
    y = sin(a)*(c5 + h*(phi(i)^2))*phi(i);
    omega_dot(i) = x + y;
end 

magnitude = sqrt(omega_dot.^2 + phi_dot.^2);

%Plot field
hold on;
quiver(phi, omega, phi_dot, omega_dot, 'red');
contour(phi, omega, magnitude, 200);
axis equal;
xlabel('Angle \phi');
ylabel('Angular Velocity \omega');
title(sprintf('Vector Field of the Roll Dynamics, a = pi/%.0f', pi/a));
hold off;

end 


function [phi, omega] = trajectory(a, phi_0, omega_0, dt, sim_time)
%
%Computes a trajectory. Requires initial conditions
%phi_0 and omega_0. Runs for length of time "sim_time"
%

global tau c5 f g h;

%Set up vectors for Euler method
time = 1:dt:sim_time;
phi = zeros(length(time),1);
omega = zeros(length(time),1);
phi(1) = phi_0;
omega(1) = omega_0;

%Euler method
for i = 1:length(time)-1
    
    phi_dot = omega(i);
    x = tau*(f + g*(phi(i)^2))*omega(i);
    y = sin(a)*(c5 + h*(phi(i)^2))*phi(i);
    omega_dot = x + y;
    
    phi(i+1) = phi(i) + dt*phi_dot;
    omega(i+1) =  omega(i) + dt*omega_dot;
    
    if phi(i+1)>1*pi
        phi(i+1) = 0;
    elseif phi(i+1)<-1*pi
        phi(i+1) = 0;
    end
    
end

end 


function omega = trajectory_quit(a, phi_0, omega_0, dt)
%
%Computes a trajectory. Requires initial conditions
%phi_0 and omega_0. Runs until phi=phi_0 again
%

global tau c5 f g h;

phi = phi_0;
omega = omega_0;

%Euler method
i = 1;
while 1==1
    
    phi_dot = omega;
    x = tau*(f + g*(phi^2))*omega;
    y = sin(a)*(c5 + h*(phi^2))*phi;
    omega_dot = x + y;
    
    phi = phi + dt*phi_dot;
    omega =  omega + dt*omega_dot;
    
    i = i+1;
    
    if abs(phi-phi_0)<0.01 && i>1/(10*dt) && omega>0
        return
    end
    
end

end


function plot_traj(xt, vt)
%
%Plots a trajectory
%

hold on;
plot(xt, vt, 'b.');
plot(xt(1), vt(1), 'go', xt(end), vt(end), 'ro');
hold off;

end 


