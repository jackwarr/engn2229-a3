function [omega_0, omega_1] = poonValues(vl, vs, vu, a, phi_0, dt)
%
%Calculates the values for the pooncarie
%

omega_0 = vl:vs:vu;
omega_1 = zeros(1,length(omega_0));

for i = 1:length(omega_0)
    omega_1(i) = trajectory_quit(a, phi_0, omega_0(i), dt);
end

end