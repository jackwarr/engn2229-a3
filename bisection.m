function bisection

% bifurcation parameter
alpha = pi/3;

% initial velocity
vl = 0.00001;
vu = 1;
vs = 0.01;

% vertical plot range
yl = 0.00001;
yu = 1;

% time step
dt = 0.001;

% set up axes
f = figure('Position',[0 0 1280 768]);
bgcolor = get(f,'color');
ax = axes('Parent',f,'position',[0.5 0.15 0.45 0.7]);

% set up interactive sliders
vlui = uicontrol('Parent',f,'Style','slider','Position',[81,154,419,23],...
                    'Callback',@vlui_callback);
vlui1 = uicontrol('Parent',f,'Style','edit','Position',[50,154,23,23],...
                    'Callback',@vlui1_callback);
vlui2 = uicontrol('Parent',f,'Style','edit','Position',[505,154,23,23],...
                    'Callback',@vlui2_callback);
vlui3 = uicontrol('Parent',f,'Style','text','Position',[190,125,200,23],...
                    'BackgroundColor',bgcolor);

vuui = uicontrol('Parent',f,'Style','slider','Position',[81,204,419,23],...
                    'Callback',@vuui_callback);
vuui1 = uicontrol('Parent',f,'Style','edit','Position',[50,204,23,23],...
                    'Callback',@vuui1_callback);
vuui2 = uicontrol('Parent',f,'Style','edit','Position',[505,204,23,23],...
                    'Callback',@vuui2_callback);
vuui3 = uicontrol('Parent',f,'Style','text','Position',[190,175,200,23],...
                    'BackgroundColor',bgcolor);

vsui = uicontrol('Parent',f,'Style','slider','Position',[81,254,419,23],...
                    'Callback',@vsui_callback);
vsui1 = uicontrol('Parent',f,'Style','text','Position',[45,250,30,23],...
                    'BackgroundColor',bgcolor);
vsui2 = uicontrol('Parent',f,'Style','text','Position',[500,250,30,23],...
                    'BackgroundColor',bgcolor);
vsui3 = uicontrol('Parent',f,'Style','text','Position',[190,225,200,23],...
                    'BackgroundColor',bgcolor);

alphaui = uicontrol('Parent',f,'Style','slider','Position',[81,304,419,23],...
                    'Callback',@alphaui_callback);
alphaui1 = uicontrol('Parent',f,'Style','text','Position',[10,300,69,23],...
                        'BackgroundColor',bgcolor);
alphaui2 = uicontrol('Parent',f,'Style','text','Position',[500,300,69,23],...
                        'BackgroundColor',bgcolor);
alphaui3 = uicontrol('Parent',f,'Style','text','Position',[240,275,100,23],...
                        'BackgroundColor',bgcolor);
            
dtui = uicontrol('Parent',f,'Style','slider','Position',[81,354,419,23],...
                    'Callback',@dtui_callback);
dtui1 = uicontrol('Parent',f,'Style','text','Position',[45,350,30,23],...
                    'BackgroundColor',bgcolor);
dtui2 = uicontrol('Parent',f,'Style','text','Position',[500,350,30,23],...
                    'BackgroundColor',bgcolor);
dtui3 = uicontrol('Parent',f,'Style','text','Position',[240,325,100,23],...
                    'BackgroundColor',bgcolor);

ylui = uicontrol('Parent',f,'Style','slider','Position',[81,404,419,23],...
                    'Callback',@ylui_callback);
ylui1 = uicontrol('Parent',f,'Style','edit','Position',[50,404,23,23],...
                    'Callback',@ylui1_callback);
ylui2 = uicontrol('Parent',f,'Style','edit','Position',[505,404,23,23],...
                    'Callback',@ylui2_callback);
ylui3 = uicontrol('Parent',f,'Style','text','Position',[190,375,200,23],...
                    'BackgroundColor',bgcolor);

yuui = uicontrol('Parent',f,'Style','slider','Position',[81,454,419,23],...
                    'Callback',@yuui_callback);
yuui1 = uicontrol('Parent',f,'Style','edit','Position',[50,454,23,23],...
                    'Callback',@yuui1_callback);
yuui2 = uicontrol('Parent',f,'Style','edit','Position',[505,454,23,23],...
                    'Callback',@yuui2_callback);
yuui3 = uicontrol('Parent',f,'Style','text','Position',[190,425,200,23],...
                    'BackgroundColor',bgcolor);

% set up push buttons
uicontrol('Parent',f,'Style','pushbutton','Position',[175,504,50,23],...
          'String','Left','Callback',@leftui_callback);
uicontrol('Parent',f,'Style','pushbutton','Position',[250,504,50,23],...
          'String','Bisect','Callback',@bisui_callback);
uicontrol('Parent',f,'Style','pushbutton','Position',[325,504,50,23],...
          'String','Right','Callback',@rightui_callback);
uicontrol('Parent',f,'Style','pushbutton','Position',[250,554,50,23],...
          'String','Undo','Callback',@undoui_callback);
histui = uicontrol('Parent',f,'Style','text','Position',[195,550,50,23],...
                    'BackgroundColor',bgcolor); 
uicontrol('Parent',f,'Style','pushbutton','Position',[325,554,50,23],...
          'String','Reset','Callback',@resetui_callback);                

% initialize history
hist = [];

% initialize GUI -  values of alpha
setui(vl,0,1,vu,0,1,vs,alpha,0,pi/2,dt,yl,0,1,yu,0,1);          

% local functions to set all GUI values          
function setui(vl,vll,vlu,vu,vul,vuu,vs,alpha,alphal,alphau,dt,...
                yl,yll,ylu,yu,yul,yuu)
    % add to history
    hist = [[vl,vll,vlu,vu,vul,vuu,vs,alpha,alphal,alphau,dt,...
                yl,yll,ylu,yu,yul,yuu];
            hist];
    set(histui,'String',num2str(size(hist,1)-1));
        
    % set values        
    set(vlui,'value',vl,'min',vll,'max',vlu);
    set(vlui1,'String',num2str(vll));
    set(vlui2,'String',num2str(vlu));
    set(vlui3,'String',sprintf('v0 lower limit = %.3f',vl));

    set(vuui,'value',vu,'min',vul, 'max',vuu);
    set(vuui1,'String',num2str(vul));
    set(vuui2,'String',num2str(vuu));
    set(vuui3,'String',sprintf('v0 upper limit = %.3f',vu));

    set(vsui,'value',log10(vs),'min',-5,'max',-1);
    set(vsui1,'String','10e-5');
    set(vsui2,'String','10e-1');
    set(vsui3,'String',sprintf('v0 step size = %.5f',vs));

    set(alphaui,'value',alpha,'min',alphal, 'max',alphau);
    set(alphaui1,'String',sprintf('%.8f',alphal));
    set(alphaui2,'String',sprintf('%.8f',alphau));
    set(alphaui3,'String',sprintf('alpha = %.8f',alpha));
            
    set(dtui,'value',log10(dt),'min',-5, 'max',-1);
    set(dtui1,'String','10e-5');
    set(dtui2,'String','10e-1');
    set(dtui3,'String',sprintf('dt = %.5f',dt));
    
    set(ylui,'value',yl,'min',yll,'max',ylu);
    set(ylui1,'String',num2str(yll));
    set(ylui2,'String',num2str(ylu));
    set(ylui3,'String',sprintf('p lower limit = %.3f',yl));
    
    set(yuui,'value',yu,'min',yul,'max',yuu);
    set(yuui1,'String',num2str(yul));
    set(yuui2,'String',num2str(yuu));
    set(yuui3,'String',sprintf('p upper limit = %.3f',yu));
    pooncarie(alpha,vl,vs,vu,dt,yl,yu,ax);
end

function setuivec(val)
    args = num2cell(val);
    setui(args{:});    
end

% local callback functions
function vlui_callback(hObject,~)
    vl = get(hObject,'Value');
    val = hist(1,:);
    val(1) = vl;
    setuivec(val);
end

function vlui1_callback(hObject,~)
    vll = str2double(get(hObject,'String'));
    vl = get(vlui,'Value');
    if vl < vll; vl = vll; end
    val = hist(1,:);
    val(1) = vl;
    val(2) = vll;
    setuivec(val);
end

function vlui2_callback(hObject,~)
    vlu = str2double(get(hObject,'String'));
    vl = get(vlui,'Value');
    if vl > vlu; vl = vlu; end
    val = hist(1,:);
    val(1) = vl;
    val(3) = vlu;
    setuivec(val);
end

function vuui_callback(hObject,~)
    vu = get(hObject,'Value');
    val = hist(1,:);
    val(4) = vu;
    setuivec(val);
end

function vuui1_callback(hObject,~)
    vul = str2double(get(hObject,'String'));
    vu = get(vuui,'Value');
    if vu < vul; vu = vul; end
    val = hist(1,:);
    val(4) = vu;
    val(5) = vul;
    setuivec(val);
end

function vuui2_callback(hObject,~)
    vuu = str2double(get(hObject,'String'));
    vu = get(vuui,'Value');
    if vu > vuu; vu = vuu; end
    val = hist(1,:);
    val(4) = vu;
    val(6) = vuu;
    setuivec(val);
end

function vsui_callback(hObject,~)
    vs = realpow(10, get(hObject,'Value'));
    val = hist(1,:);
    val(7) = vs;
    setuivec(val);
end

function alphaui_callback(hObject,~)
    alpha = get(hObject,'Value');
    val = hist(1,:);
    val(8) = alpha;
    setuivec(val);
end

function dtui_callback(hObject,~)
    dt = realpow(10, get(hObject,'Value'));
    val = hist(1,:);
    val(11) = dt;
    setuivec(val);
end

function ylui_callback(hObject,~)
    yl = get(hObject,'Value');
    val = hist(1,:);
    val(12) = yl;
    setuivec(val);
end

function ylui1_callback(hObject,~)
    yll = str2double(get(hObject,'String'));
    yl = get(ylui,'Value');
    if yl < yll; yl = yll; end
    val = hist(1,:);
    val(12) = yl;
    val(13) = yll;
    setuivec(val);
end

function ylui2_callback(hObject,~)
    ylu = str2double(get(hObject,'String'));
    yl = get(ylui,'Value');
    if yl > ylu; yl = ylu; end
    val = hist(1,:);
    val(12) = yl;
    val(14) = ylu;
    setuivec(val);
end

function yuui_callback(hObject,~)
    yu = get(hObject,'Value');
    val = hist(1,:);
    val(15) = yu;
    setuivec(val);
end

function yuui1_callback(hObject,~)
    yul = str2double(get(hObject,'String'));
    yu = get(yuui,'Value');
    if yu < yul; yu = yul; end
    val = hist(1,:);
    val(15) = yu;
    val(16) = yul;
    setuivec(val);
end

    function yuui2_callback(hObject,~)
        yuu = str2double(get(hObject,'String'));
        yu = get(yuui,'Value');
        if yu > yuu; yu = yuu; end
        val = hist(1,:);
        val(15) = yu;
        val(17) = yuu;
        setuivec(val);
    end

    function leftui_callback(~,~)
        al = get(alphaui,'min');
        au = get(alphaui,'max');
        alpha = al+(au-al)/2;
        val = hist(1,:);
        val(8) = alpha;
        val(10) = alpha;
        setuivec(val);
    end

    function bisui_callback(~,~)
        al = get(alphaui,'min');
        au = get(alphaui,'max');
        alpha = al+(au-al)/2;
        val = hist(1,:);
        val(8) = alpha;
        setuivec(val);
    end

    function rightui_callback(~,~)
        al = get(alphaui,'min');
        au = get(alphaui,'max');
        alpha = al+(au-al)/2;
        val = hist(1,:);
        val(8) = alpha;
        val(9) = alpha;
        setuivec(val);
    end

    function undoui_callback(~,~)
        hs = size(hist,1);
        if hs > 1
            val = hist(2,:);
            hist = hist(3:end,:);
            setuivec(val);
        end
    end

    function resetui_callback(~,~)
        val = hist(end, :);
        hist = [];
        setuivec(val);   %takes val and puts it into individual arguments to call pooncarie
    end


end